import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Transformer } from './transformer.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Transformer>;

@Injectable()
export class TransformerService {

    private resourceUrl =  SERVER_API_URL + 'microservice/api/transformers';

    constructor(private http: HttpClient) { }

    create(transformer: Transformer): Observable<EntityResponseType> {
        const copy = this.convert(transformer);
        return this.http.post<Transformer>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(transformer: Transformer): Observable<EntityResponseType> {
        const copy = this.convert(transformer);
        return this.http.put<Transformer>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Transformer>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Transformer[]>> {
        const options = createRequestOption(req);
        return this.http.get<Transformer[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Transformer[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Transformer = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Transformer[]>): HttpResponse<Transformer[]> {
        const jsonResponse: Transformer[] = res.body;
        const body: Transformer[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Transformer.
     */
    private convertItemFromServer(transformer: Transformer): Transformer {
        const copy: Transformer = Object.assign({}, transformer);
        return copy;
    }

    /**
     * Convert a Transformer to a JSON which can be sent to the server.
     */
    private convert(transformer: Transformer): Transformer {
        const copy: Transformer = Object.assign({}, transformer);
        return copy;
    }
}
