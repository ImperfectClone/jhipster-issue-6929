import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../shared';
import {
    TransformerService,
    TransformerPopupService,
    TransformerComponent,
    TransformerDetailComponent,
    TransformerDialogComponent,
    TransformerPopupComponent,
    TransformerDeletePopupComponent,
    TransformerDeleteDialogComponent,
    transformerRoute,
    transformerPopupRoute,
} from './';

const ENTITY_STATES = [
    ...transformerRoute,
    ...transformerPopupRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TransformerComponent,
        TransformerDetailComponent,
        TransformerDialogComponent,
        TransformerDeleteDialogComponent,
        TransformerPopupComponent,
        TransformerDeletePopupComponent,
    ],
    entryComponents: [
        TransformerComponent,
        TransformerDialogComponent,
        TransformerPopupComponent,
        TransformerDeleteDialogComponent,
        TransformerDeletePopupComponent,
    ],
    providers: [
        TransformerService,
        TransformerPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayTransformerModule {}
