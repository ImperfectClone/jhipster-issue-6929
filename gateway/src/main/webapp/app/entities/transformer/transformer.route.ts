import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TransformerComponent } from './transformer.component';
import { TransformerDetailComponent } from './transformer-detail.component';
import { TransformerPopupComponent } from './transformer-dialog.component';
import { TransformerDeletePopupComponent } from './transformer-delete-dialog.component';

export const transformerRoute: Routes = [
    {
        path: 'transformer',
        component: TransformerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Transformers'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'transformer/:id',
        component: TransformerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Transformers'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const transformerPopupRoute: Routes = [
    {
        path: 'transformer-new',
        component: TransformerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Transformers'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transformer/:id/edit',
        component: TransformerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Transformers'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'transformer/:id/delete',
        component: TransformerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Transformers'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
