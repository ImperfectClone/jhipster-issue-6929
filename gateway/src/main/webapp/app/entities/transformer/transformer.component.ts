import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Transformer } from './transformer.model';
import { TransformerService } from './transformer.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-transformer',
    templateUrl: './transformer.component.html'
})
export class TransformerComponent implements OnInit, OnDestroy {
transformers: Transformer[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private transformerService: TransformerService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.transformerService.query().subscribe(
            (res: HttpResponse<Transformer[]>) => {
                this.transformers = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTransformers();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Transformer) {
        return item.id;
    }
    registerChangeInTransformers() {
        this.eventSubscriber = this.eventManager.subscribe('transformerListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
