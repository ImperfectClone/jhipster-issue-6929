package io.jharris.issue6929.gateway.config;

import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "io.jharris.issue6929.gateway")
public class FeignConfiguration {

}
