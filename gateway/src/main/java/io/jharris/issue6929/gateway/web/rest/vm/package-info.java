/**
 * View Models used by Spring MVC REST controllers.
 */
package io.jharris.issue6929.gateway.web.rest.vm;
