/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { GatewayTestModule } from '../../../test.module';
import { TransformerDetailComponent } from '../../../../../../main/webapp/app/entities/transformer/transformer-detail.component';
import { TransformerService } from '../../../../../../main/webapp/app/entities/transformer/transformer.service';
import { Transformer } from '../../../../../../main/webapp/app/entities/transformer/transformer.model';

describe('Component Tests', () => {

    describe('Transformer Management Detail Component', () => {
        let comp: TransformerDetailComponent;
        let fixture: ComponentFixture<TransformerDetailComponent>;
        let service: TransformerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [TransformerDetailComponent],
                providers: [
                    TransformerService
                ]
            })
            .overrideTemplate(TransformerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TransformerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TransformerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Transformer(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.transformer).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
