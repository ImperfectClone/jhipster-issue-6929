/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../test.module';
import { TransformerComponent } from '../../../../../../main/webapp/app/entities/transformer/transformer.component';
import { TransformerService } from '../../../../../../main/webapp/app/entities/transformer/transformer.service';
import { Transformer } from '../../../../../../main/webapp/app/entities/transformer/transformer.model';

describe('Component Tests', () => {

    describe('Transformer Management Component', () => {
        let comp: TransformerComponent;
        let fixture: ComponentFixture<TransformerComponent>;
        let service: TransformerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [TransformerComponent],
                providers: [
                    TransformerService
                ]
            })
            .overrideTemplate(TransformerComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TransformerComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TransformerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Transformer(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.transformers[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
