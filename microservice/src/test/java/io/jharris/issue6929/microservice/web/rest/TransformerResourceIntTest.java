package io.jharris.issue6929.microservice.web.rest;

import io.jharris.issue6929.microservice.MicroserviceApp;

import io.jharris.issue6929.microservice.domain.Transformer;
import io.jharris.issue6929.microservice.repository.TransformerRepository;
import io.jharris.issue6929.microservice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static io.jharris.issue6929.microservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TransformerResource REST controller.
 *
 * @see TransformerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MicroserviceApp.class)
public class TransformerResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_POWER = 1L;
    private static final Long UPDATED_POWER = 2L;

    @Autowired
    private TransformerRepository transformerRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTransformerMockMvc;

    private Transformer transformer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TransformerResource transformerResource = new TransformerResource(transformerRepository);
        this.restTransformerMockMvc = MockMvcBuilders.standaloneSetup(transformerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Transformer createEntity(EntityManager em) {
        Transformer transformer = new Transformer()
            .name(DEFAULT_NAME)
            .power(DEFAULT_POWER);
        return transformer;
    }

    @Before
    public void initTest() {
        transformer = createEntity(em);
    }

    @Test
    @Transactional
    public void createTransformer() throws Exception {
        int databaseSizeBeforeCreate = transformerRepository.findAll().size();

        // Create the Transformer
        restTransformerMockMvc.perform(post("/api/transformers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transformer)))
            .andExpect(status().isCreated());

        // Validate the Transformer in the database
        List<Transformer> transformerList = transformerRepository.findAll();
        assertThat(transformerList).hasSize(databaseSizeBeforeCreate + 1);
        Transformer testTransformer = transformerList.get(transformerList.size() - 1);
        assertThat(testTransformer.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTransformer.getPower()).isEqualTo(DEFAULT_POWER);
    }

    @Test
    @Transactional
    public void createTransformerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = transformerRepository.findAll().size();

        // Create the Transformer with an existing ID
        transformer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransformerMockMvc.perform(post("/api/transformers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transformer)))
            .andExpect(status().isBadRequest());

        // Validate the Transformer in the database
        List<Transformer> transformerList = transformerRepository.findAll();
        assertThat(transformerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllTransformers() throws Exception {
        // Initialize the database
        transformerRepository.saveAndFlush(transformer);

        // Get all the transformerList
        restTransformerMockMvc.perform(get("/api/transformers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transformer.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].power").value(hasItem(DEFAULT_POWER.intValue())));
    }

    @Test
    @Transactional
    public void getTransformer() throws Exception {
        // Initialize the database
        transformerRepository.saveAndFlush(transformer);

        // Get the transformer
        restTransformerMockMvc.perform(get("/api/transformers/{id}", transformer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(transformer.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.power").value(DEFAULT_POWER.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTransformer() throws Exception {
        // Get the transformer
        restTransformerMockMvc.perform(get("/api/transformers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTransformer() throws Exception {
        // Initialize the database
        transformerRepository.saveAndFlush(transformer);
        int databaseSizeBeforeUpdate = transformerRepository.findAll().size();

        // Update the transformer
        Transformer updatedTransformer = transformerRepository.findOne(transformer.getId());
        // Disconnect from session so that the updates on updatedTransformer are not directly saved in db
        em.detach(updatedTransformer);
        updatedTransformer
            .name(UPDATED_NAME)
            .power(UPDATED_POWER);

        restTransformerMockMvc.perform(put("/api/transformers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTransformer)))
            .andExpect(status().isOk());

        // Validate the Transformer in the database
        List<Transformer> transformerList = transformerRepository.findAll();
        assertThat(transformerList).hasSize(databaseSizeBeforeUpdate);
        Transformer testTransformer = transformerList.get(transformerList.size() - 1);
        assertThat(testTransformer.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTransformer.getPower()).isEqualTo(UPDATED_POWER);
    }

    @Test
    @Transactional
    public void updateNonExistingTransformer() throws Exception {
        int databaseSizeBeforeUpdate = transformerRepository.findAll().size();

        // Create the Transformer

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTransformerMockMvc.perform(put("/api/transformers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(transformer)))
            .andExpect(status().isCreated());

        // Validate the Transformer in the database
        List<Transformer> transformerList = transformerRepository.findAll();
        assertThat(transformerList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTransformer() throws Exception {
        // Initialize the database
        transformerRepository.saveAndFlush(transformer);
        int databaseSizeBeforeDelete = transformerRepository.findAll().size();

        // Get the transformer
        restTransformerMockMvc.perform(delete("/api/transformers/{id}", transformer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Transformer> transformerList = transformerRepository.findAll();
        assertThat(transformerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Transformer.class);
        Transformer transformer1 = new Transformer();
        transformer1.setId(1L);
        Transformer transformer2 = new Transformer();
        transformer2.setId(transformer1.getId());
        assertThat(transformer1).isEqualTo(transformer2);
        transformer2.setId(2L);
        assertThat(transformer1).isNotEqualTo(transformer2);
        transformer1.setId(null);
        assertThat(transformer1).isNotEqualTo(transformer2);
    }
}
