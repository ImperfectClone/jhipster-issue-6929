package io.jharris.issue6929.microservice.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class for representing a Robot in Disguise.
 */
@ApiModel(description = "Class for representing a Robot in Disguise.")
@Entity
@Table(name = "transformer")
public class Transformer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * The name of the Transformer.
     */
    @ApiModelProperty(value = "The name of the Transformer.")
    @Column(name = "name")
    private String name;

    /**
     * The power level of the Transformer.
     */
    @ApiModelProperty(value = "The power level of the Transformer.")
    @Column(name = "power")
    private Long power;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Transformer name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPower() {
        return power;
    }

    public Transformer power(Long power) {
        this.power = power;
        return this;
    }

    public void setPower(Long power) {
        this.power = power;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Transformer transformer = (Transformer) o;
        if (transformer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), transformer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Transformer{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", power=" + getPower() +
            "}";
    }
}
