/**
 * Specific errors used with Zalando's "problem-spring-web" library.
 *
 * More information on https://github.com/zalando/problem-spring-web
 */
package io.jharris.issue6929.microservice.web.rest.errors;
